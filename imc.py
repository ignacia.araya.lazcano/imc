import sys

peso = int(sys.argv[1])
altura = int(sys.argv[2])/100

imc = peso / ((altura)**2)

print(f"Su IMC es {imc:.2f}")

if imc < 18.5:
    print("La clasificación OMS es Bajo Peso")
elif 18.5 <= imc < 25:
    print("La clasificación OMS es Adecuado")
elif 25 <= imc < 30:
    print("La clasificación OMS es Sobrepeso")
elif 30 <= imc < 35:
    print("La clasificación OMS es Obesidad Grado I")
elif 35 <= imc < 40:
    print("La clasificación OMS es Obesidad Grado II")
elif imc > 40 :
    print("La clasificación OMS es Obesidad Grado III")
